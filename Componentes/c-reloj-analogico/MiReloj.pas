{
********************************************************************
********************************************************************
El blog de Delphi B�sico - 2017
Autor: Salvador Jover

El c�digo forma parte de una serie de entradas del blog de Delphi 
B�sico, con fines exclusivamente did�cticos y se entrega tal cual; 
no tiene car�cter comercial ni ofrece garant�as de que funcione fuera 
del uso meramente educativo. 

Se ha desarrollado con la version Delphi 10.1 Berlin Starter
y mi recomendaci�n es la de que sigas los pasos indicados en la serie
partiendo de un proyecto nuevo, y agregando los m�dulos necesarios,
principalmente el fichero (PAS) o el fichero (RES), si no deseas escribirlos
o generar la compilaci�n del recurso. 

Introducci�n a la serie: 
https://community.embarcadero.com/blogs/entry/modulo-de-control-de-presencia-2017-de-la-vcl-a-fmx
Cap�tulos:
https://delphibasico.com/2017/01/31/modulo-c-p-2017-i-reloj-analogico/
y siguientes (en el blog principal).

Durante el transcurso de la serie, existe un taller de trabajos, sobre
la misma, como ayuda, en el area de grupos de Embarcadero, creado a tal
efecto de acompa�arla. 
https://community.embarcadero.com/my-groups/viewgroup/580-taller-de-delphi-basico-2017
Es necesario registrarse para el acceso.

Espero que os guste.
********************************************************************
********************************************************************
}


unit MiReloj;

interface

uses
  System.SysUtils, System.Classes, FMX.Types, FMX.Controls, FMX.Objects,
FMX.Graphics, System.UITypes, System.UIConsts, System.Types;

type
  TTipo = (maSegundos, maMinutos, maHoras);

  TParteAdorno = (paExtra,
                  paLogo,
                  paDialSegundos,
                  paDialMinutos,
                  paDialHoras,
                  paEsfera,
                  paBoton,
                  paAutor,
                  paMarca);

  TAdornos = Set of TParteAdorno;

  TAdornosProp = class(TPersistent)
   private
    FAutor: String;
    FImage: TBitmap;
    FMarca: String;
    FOnNeedRepaint: TNotifyEvent;
    FDistanceToCenterAutor: Integer;
    FDistanceToCenterMarca: Integer;
    procedure SetAutor(const Value: String);
    procedure SetFImage(const Value: TBitmap);
    procedure SetMarca(const Value: String);
    procedure SetOnNeedRepaint(const Value: TNotifyEvent);
    procedure SetDistanceToCenterAutor(const Value: Integer);
    procedure SetDistanceToCenterMarca(const Value: Integer);
   protected
    procedure DoNeedParentRepaint; virtual;
   public
    constructor Create;
    destructor Destroy; override;
    property OnNeedRepaint: TNotifyEvent read FOnNeedRepaint write SetOnNeedRepaint;
   published
    property DistanceToCenterAutor: Integer read FDistanceToCenterAutor write SetDistanceToCenterAutor default 115;
    property DistanceToCenterMarca: Integer read FDistanceToCenterMarca write SetDistanceToCenterMarca default 75;
    property Autor: String read FAutor write SetAutor;
    property Image: TBitmap read FImage write SetFImage;
    property Marca: String read FMarca write SetMarca;
  end;

  TManecilla = class(TLine)
  private
    FTipoManecilla: TTipo;
    FPorcentajeSobreRadio: Single;
    procedure SetPorcentajeSobreRadio(const Value: Single);
  protected
   procedure ParentChanged; override;
  public
   constructor CreateManecilla(AOwner: TComponent; ATipoManecilla: TTipo); virtual;
   procedure SetAnguloRotacion(const AHoraActual: TDateTime);
   procedure AjustarPosicion(ANewWidth, ANewHeight: Single);
  published
   property Tipo: TTipo read FTipoManecilla;
   property PorcentajeSobreRadio: Single read FPorcentajeSobreRadio write SetPorcentajeSobreRadio;
  end;

  TMiReloj = class(TCircle)
  private
    { Private declarations }
    FColorBEsfera: TAlphaColor;
    FColorExtra: TAlphaColor;
    FDateTime: TDateTime;
    FRadioEsfera: Single;
    FInternalTimer: TTimer;
    FEnableTimer: Boolean;
    FOnInternalTimer: TNotifyEvent;
    FPath: TPathData;
    FAdornos: TAdornos;
    FAdornosProp: TAdornosProp;
    FMargenEsfera: SmallInt;
    function CreaManecilla(const ATipoManecilla: TTipo;const ANombre: String; APorcentajeRadio: Single): TManecilla;
    function CreaTemporizador(FuncTemporiza: TNotifyEvent): TTimer;
   // procedure SetAutor(const Value: String);
    procedure SetColorBEsfera(const Value: TAlphaColor);
    procedure SetColorExtra(const Value: TAlphaColor);
  //  procedure SetMarca(const Value: String);
    procedure SetEnableTimer(const Value: Boolean);
    procedure TimerOnInternalTimer(Sender: TObject);
    procedure SetAdornos(const Value: TAdornos);
    procedure SetMargenEsfera(const Value: SmallInt);
    procedure SetAdornosProp(const Value: TAdornosProp);
    procedure OnNeedRepaint(Sender: TObject);
  strict protected
    FMHoras: TManecilla;
    FMMinutos: TManecilla;
    FMSegundos: TManecilla;
  protected
    { Protected declarations }
    procedure CreateDial(const ATipoDial: TTipo); virtual;
    procedure Paint; override;
    procedure PintarAdornoExtra(const APintar: Boolean); virtual;
    procedure PintarBotonCentral(const APintar: Boolean); virtual;
    procedure PintarDial(const ATipoDial: TTipo; const APintar: Boolean); virtual;
    procedure PintarEsfera(const APintar: Boolean); virtual;
    procedure PintarLogo(const APintar: Boolean); virtual;
    procedure PintarTextoAutor(const APintar: Boolean); virtual;
    procedure PintarTextoMarca(const APintar: Boolean); virtual;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(X,Y, AWidth, AHeight: Single); override;
    procedure SetNewTime(const ANow: TDateTime);
    property DateTimeNow: TDateTime read FDateTime;
  published
    { Published declarations }
    property Fill;
    property Stroke;
    property OnKeyDown;
    property Adornos: TAdornos read FAdornos write SetAdornos default [paDialSegundos, paDialMinutos, paDialHoras] ;
    property AdornosProp: TAdornosProp read FAdornosProp write SetAdornosProp;
    //property Autor: String read FAutor write SetAutor;
    property ColorBEsfera: TAlphaColor read FColorBEsfera write SetColorBEsfera default claBlue;
    property ColorExtra: TAlphaColor read FColorExtra write SetColorExtra default claWhite;
    property EnableTimer: Boolean read FEnableTimer write SetEnableTimer default True;
    //property Marca: String read FMarca write SetMarca;
    property MargenEsfera: SmallInt read FMargenEsfera write SetMargenEsfera default 20;
    property OnInternalTimer: TNotifyEvent read FOnInternalTimer write FOnInternalTimer;
  end;

procedure Register;

implementation

uses System.Math, DateUtils;

{$R shapereloj.res}

procedure Register;
begin
  RegisterComponents('Samples', [TMiReloj]);
end;

function TMiReloj.CreaManecilla(const ATipoManecilla: TTipo;const ANombre: String; APorcentajeRadio: Single): TManecilla;
begin
  Result:= TManecilla.CreateManecilla(self, ATipoManecilla);
  with Result do
  begin
    Parent:= Self;
    Name:= ANombre;
    PorcentajeSobreRadio:= APorcentajeRadio;
    Stored:= False;
  end;
end;

constructor TMiReloj.Create(AOwner: TComponent);
begin
  inherited;
  FAdornosProp:= TAdornosProp.Create;
  FAdornosProp.OnNeedRepaint:= OnNeedRepaint;
  //ajustamos margenes por defecto
  Margins.Bottom:= 10;
  Margins.Left:= 10;
  Margins.Right:= 10;
  Margins.Top:= 10;

  FMargenEsfera:= 20;

  Width:= 400;
  Height:= 400;

  FMHoras:= CreaManecilla(maHoras, 'manecilla_horas', 40.0);
  FMMinutos:= CreaManecilla(maMinutos, 'manecilla_minutos', 55.0);
  FMSegundos:= CreaManecilla(maSegundos, 'manecilla_segundos', 70.0);

  FAdornos:= [paDialSegundos, paDialMinutos, paDialHoras];

  FPath := TPathData.Create;

  FColorBEsfera:= claBlue;
  FColorExtra:= claWhite;

  FEnableTimer:= True;
  FInternalTimer:= CreaTemporizador(TimerOnInternalTimer);
  TimerOnInternalTimer(nil);
end;

procedure TMiReloj.CreateDial(const ATipoDial: TTipo);

  procedure GoToAVertex(n: Integer; p: Integer; Angle, CircumRadius: Double;
    const ATipoDial: TTipo; IsLineTo: Boolean = True);
  var
    NewLocation: TPointF;
    FLongDial: Integer;
  begin            //punto centro
    NewLocation.X := (Width  / 2) + (Sin(n * Angle) * CircumRadius);
    NewLocation.Y := (Height / 2) - (Cos(n * Angle) * CircumRadius);

    case ATipoDial of
      TTipo.maSegundos: FLongDial:= 5;
      TTipo.maMinutos: FLongDial:= 7;
      TTipo.maHoras: FLongDial:= 10;
    else
      FLongDial:= 0;
    end;

    if IsLineTo then
    begin
      FPath.MoveTo(NewLocation);
      NewLocation.X:= NewLocation.X +  (Sin(n * Angle) * FLongDial);
      NewLocation.Y:= NewLocation.Y -  (Cos(n * Angle) * FLongDial);
      if n <= p then FPath.LineTo(NewLocation);
    end
    else
      NewLocation.X:= NewLocation.X +  (Sin(n * Angle) * FLongDial);
  end;

var
  i: Integer;
  Angle: Double;
  FNumberOfSides: Integer;
begin
  case ATipoDial of
    TTipo.maSegundos: FNumberOfSides:= 60;
    TTipo.maMinutos: FNumberOfSides:= 60;
    TTipo.maHoras: FNumberOfSides:= 12;
  else
    FNumberOfSides:= 1;
  end;
  Angle:=  2 * PI / FNumberOfSides;

  FPath.Clear;

  GoToAVertex(0, FNumberOfSides, Angle, FRadioEsfera, ATipoDial, False);
  for i := 1 to FNumberOfSides + 1 do
    GoToAVertex(i, FNumberOfSides, Angle, FRadioEsfera, ATipoDial);

  FPath.ClosePath;
end;

function TMiReloj.CreaTemporizador(FuncTemporiza: TNotifyEvent): TTimer;
begin
  Result:= TTimer.Create(nil);
  with Result do
  begin
    Enabled:= FEnableTimer;
    OnTimer:= FuncTemporiza;
  end;
end;

destructor TMiReloj.Destroy;
begin
  FAdornosProp.Free;
  FInternalTimer.Free;
  FPath.Free;
  inherited;
end;

procedure TMiReloj.OnNeedRepaint(Sender: TObject);
begin
  Repaint;
end;

{$REGION 'Metodos de escritura campos'}

procedure TMiReloj.SetColorBEsfera(const Value: TAlphaColor);
begin
  if Value <> FColorBEsfera then
  begin
    FColorBEsfera := Value;
    Repaint;
  end;
end;

procedure TMiReloj.SetColorExtra(const Value: TAlphaColor);
begin
  if Value <> FColorExtra then
  begin
    FColorExtra := Value;
    Repaint;
  end;
end;

procedure TMiReloj.SetAdornos(const Value: TAdornos);
begin
  if FAdornos <> Value then
  begin
    FAdornos := Value;
    Repaint;
  end;
end;

procedure TMiReloj.SetAdornosProp(const Value: TAdornosProp);
  function Equals(A, B: TAdornosProp): Boolean;
  begin
    Result:= (CompareStr(A.Autor, B.Autor) = 0) and
             (CompareStr(A.Marca, B.Marca) = 0) and
             A.Image.EqualsBitmap(B.Image);
  end;
begin
  if not Equals(FAdornosProp, Value) then
  begin
    FAdornosProp.Autor := Value.Autor;
    FAdornosProp.Marca:= Value.Marca;
    if Assigned(Value.Image) then
      FAdornosProp.Image.Assign(Value.Image)
    else
     begin
       FAdornosProp.Image.Free;
       FAdornosProp.Image:= Nil;
     end;
  end;
  Repaint;
end;

procedure TMiReloj.SetEnableTimer(const Value: Boolean);
begin
  if FEnableTimer <> Value then
  begin
    FEnableTimer := Value;
    FInternalTimer.Enabled:= FEnableTimer;
    Repaint;
  end;
end;

procedure TMiReloj.SetMargenEsfera(const Value: SmallInt);
begin
  if FMargenEsfera <> Value then
  begin
    FMargenEsfera := Value;
    Repaint;
  end;
end;

{$ENDREGION}

procedure TMiReloj.SetNewTime(const ANow: TDateTime);
begin
  //actualizamos la hora
  FDateTime:= ANow;
  //comunicamos a cada manecilla para que corrijan al angulo adecuado
  FMSegundos.SetAnguloRotacion(FDateTime);
  FMMinutos.SetAnguloRotacion(FDateTime);
  FMHoras.SetAnguloRotacion(FDateTime);

  Repaint;
end;


{$REGION 'Partes pintadas en el interior del reloj'}

procedure TMiReloj.PintarAdornoExtra(const APintar: Boolean);
var
  fRect: TRectF;
begin
  if not APintar then Exit;

  Canvas.Fill.Color:= FColorExtra;
  Canvas.Stroke.Color:= claGrey;
  fRect:= TRectF.Create((width-Min(Width, Height))/2+10,
                        (height)/2+100,
                        (width+Min(Width, Height))/2-10,
                        (height)/2-100);
   Canvas.DrawRect(fRect, 100, 100, AllCorners, 50);
   Canvas.FillRect(fRect, 100, 100, AllCorners, 50);
end;

procedure TMiReloj.PintarBotonCentral(const APintar: Boolean);
var
  fRect: TRectF;
begin
  if not APintar then Exit;

  //dibujamos el punto central
  fRect:= TRectF.Create(width/2-4,
                        height/2-4,
                        width/2+4,
                        height/2+4);
  Canvas.Stroke.Thickness:= 3;
  Canvas.Stroke.Color:= claBlue;
  Canvas.Fill.Kind:= TBrushKind.Solid;
  Canvas.Fill.Color:= claBlack;
  Canvas.FillEllipse(fRect, AbsoluteOpacity);
  Canvas.DrawEllipse(fRect, AbsoluteOpacity);
end;

procedure TMiReloj.PintarDial(const ATipoDial: TTipo; const APintar: Boolean);
begin
  if not APintar then Exit;

  case ATipoDial of
    maSegundos: begin
                  CreateDial(maSegundos);
                  Canvas.Stroke.Thickness:= 1;
                  Canvas.FillPath(FPath, AbsoluteOpacity);
                  Canvas.DrawPath(FPath, AbsoluteOpacity);
                end;
    maMinutos :;
    maHoras   : begin
                  CreateDial(maHoras);
                  Canvas.Stroke.Thickness:= 4;
                  Canvas.Stroke.Color:= claGrey;
                  Canvas.FillPath(FPath, AbsoluteOpacity);
                  Canvas.DrawPath(FPath, AbsoluteOpacity);
                end;
  end;
end;

procedure TMiReloj.PintarEsfera(const APintar: Boolean);
var
  fRect: TRectF;
begin
  if not APintar then Exit;

  fRect:= TRectF.Create((width-Min(Width, Height))/2+5,
                        (height-Min(Width, Height))/2+5,
                        (width+Min(Width, Height))/2-5,
                        (height+Min(Width, Height))/2-5);
  Canvas.Stroke.Kind:= TBrushKind.Gradient;
  Canvas.Stroke.Thickness:= 12;
  Canvas.Stroke.Color:= FColorBEsfera;
  Canvas.DrawEllipse(fRect, AbsoluteOpacity);
end;

procedure TMiReloj.PintarLogo(const APintar: Boolean);
var
  fRect: TRectF;
begin
  if (not APintar) then Exit;

  if Assigned(FAdornosProp) and Assigned(FAdornosProp.Image) then
  begin
    fRect:= TRectF.Create((width-(AdornosProp.Image.Width/2))/2,
                          (height-(AdornosProp.Image.Height/2))/2,
                          (width+(AdornosProp.Image.Width/2))/2,
                          (height+(AdornosProp.Image.Height/2))/2);
    Canvas.DrawBitmap(FAdornosProp.Image, RectF(0,0,FAdornosProp.Image.Width,FAdornosProp.Image.Height), fRect, 20);
  end;
end;

procedure TMiReloj.PintarTextoAutor(const APintar: Boolean);
var
  fRect: TRectF;
begin
  if not APintar then Exit;

  if Assigned(FAdornosProp) then
  begin
    fRect:= TRectF.Create((width-Min(Width, Height))/2,
                          FAdornosProp.DistanceToCenterAutor + (height-Min(Width, Height))/2,
                          (width+Min(Width, Height))/2,
                          (height+Min(Width, Height))/2);
    Canvas.Stroke.Color:= claBlack;
    Canvas.Fill.Kind:= TBrushKind.Solid;
    Canvas.Fill.Color:= claBlack;
    Canvas.Font.Family:= 'Segoe Print';
    Canvas.Font.Size:= 10;
    Canvas.FillText(fRect, FAdornosProp.Autor, False, AbsoluteOpacity, [], TTextAlign.Center, TTextAlign.Center);
  end;
end;

procedure TMiReloj.PintarTextoMarca(const APintar: Boolean);
var
  fRect: TRectF;
begin
  if not APintar then Exit;

  if Assigned(FAdornosProp) then
  begin
    fRect:= TRectF.Create((width-Min(Width, Height))/2,
                          FAdornosProp.DistanceToCenterMarca + (height-Min(Width, Height))/2,
                          (width+Min(Width, Height))/2,
                          (height+Min(Width, Height))/2);
    Canvas.Stroke.Color:= claBlack;
    Canvas.Fill.Kind:= TBrushKind.Solid;
    Canvas.Fill.Color:= claBlack;
    Canvas.Font.Family:= 'Segoe UI';
    Canvas.Font.Size:= 14;
    Canvas.FillText(fRect, FAdornosProp.Marca, False, AbsoluteOpacity, [], TTextAlign.Center, TTextAlign.Center);
  end;
end;

{$ENDREGION}

procedure TMiReloj.TimerOnInternalTimer(Sender: TObject);
begin
  SetNewTime(Now);
  if Assigned(FOnInternalTimer) then FOnInternalTimer(Self);
end;

procedure TMiReloj.SetBounds(X,Y, AWidth, AHeight: Single);
begin
  inherited SetBounds(X,Y,AWidth,AHeight);

  FRadioEsfera:= Min(ShapeRect.Width / 2, ShapeRect.Height / 2)- MargenEsfera;

  with FMSegundos do
  begin
    AjustarPosicion(AWidth, AHeight);
    Width:= FRadioEsfera * PorcentajeSobreRadio / 100;
    Height:= FRadioEsfera * PorcentajeSobreRadio / 100;
  end;

  with FMMinutos do
  begin
    AjustarPosicion(AWidth, AHeight);
    Width:= FRadioEsfera * PorcentajeSobreRadio / 100;
    Height:= FRadioEsfera * PorcentajeSobreRadio / 100;
  end;

  with FMHoras do
  begin
    AjustarPosicion(AWidth, AHeight);
    Width:= FRadioEsfera * PorcentajeSobreRadio / 100;
    Height:= FRadioEsfera * PorcentajeSobreRadio / 100;
  end;
end;

procedure TMiReloj.Paint;
begin
  inherited;
  //pintamos un adorno extra
  PintarAdornoExtra(paExtra in FAdornos);
  //dibujamos la imagen
  PintarLogo(paLogo in FAdornos);
  //pintamos los diales
  PintarDial(maSegundos, paDialSegundos in FAdornos);
  PintarDial(maHoras, paDialHoras in FAdornos);
  //dibujamos una linea de esfera exterior
  PintarEsfera(paEsfera in FAdornos);
  //dibujamos el punto central de union de agujas
  PintarBotonCentral(paBoton in FAdornos);
  //rellenamos el texto de la marca
  PintarTextoMarca((Trim(FAdornosProp.Marca) <> '') and (paMarca in FAdornos));
  //rellenamos el autor
  PintarTextoAutor((Trim(FAdornosProp.Autor) <> '') and (paAutor in FAdornos));
end;

{ TManecilla }

constructor TManecilla.CreateManecilla(AOwner: TComponent; ATipoManecilla: TTipo);
begin
  inherited Create(AOwner);
  FTipoManecilla:= ATipoManecilla;
  with RotationCenter do
  begin
    X:= 0;
    Y:= 0;
  end;
  Stroke.Kind:= TBrushKind.Gradient;
    case FTipoManecilla of
      maSegundos: begin
                    Stroke.Color:= claGreen;
                    Stroke.Thickness:= 2;
                  end;
      maMinutos : begin
                    Stroke.Color:= claBlue;
                    Stroke.Thickness:= 5;
                  end;
      maHoras   : begin
                    Stroke.Color:= claBlue;
                    Stroke.Thickness:= 5;
                  end;
    end;
end;

procedure TManecilla.ParentChanged;
begin
  inherited;
  if (Parent <> nil) and (Parent is TMiReloj) then
  begin
    Position.x := (Parent as TMiReloj).ShapeRect.Width/2;
    Position.y:= (Parent as TMiReloj).ShapeRect.Height/2;
  end;
end;

procedure TManecilla.AjustarPosicion(ANewWidth, ANewHeight: Single);
begin
  Position.x := ANewWidth/2;
  Position.y:= ANewHeight/2;
end;

procedure TManecilla.SetAnguloRotacion(const AHoraActual: TDateTime);
var
  FRotacionSegundos: Real;
  FRotacionMinutos: Real;
  FRotacionHoras: Real;
  FTemp: Word;
begin
  case FTipoManecilla of
    maSegundos: begin
                  FTemp:= SecondOfTheMinute(AHoraActual);
                  FRotacionSegundos:= 225 + (FTemp) * 6;
                  RotationAngle:= FRotacionSegundos;
                end;
    maMinutos : begin
                  FTemp:= MinuteOfTheHour(AHoraActual);
                  FRotacionMinutos:= 225 +  (FTemp) * 6;
                  RotationAngle:= FRotacionMinutos;
                end;
    maHoras   : begin
                  FTemp:= HourOfTheDay(AHoraActual);
                  FRotacionHoras:= 225 + ((FTemp) * 30);
                  FTemp:= MinuteOfTheHour(AHoraActual);
                  FRotacionHoras:= FRotacionHoras + (FTemp*0.5);
                  RotationAngle:= FRotacionHoras;
                end;
  end;
end;

procedure TManecilla.SetPorcentajeSobreRadio(const Value: Single);
begin
  FPorcentajeSobreRadio := Value;
end;

{ TAdornosProp }

constructor TAdornosProp.Create;
var
  InStream: TResourceStream;
begin
  inherited;
  InStream := TResourceStream.Create(HInstance, 'RES_LOGO_150X150', RT_RCDATA);
  try
    FImage:= TBitmap.CreateFromStream(InStream);
  finally
    InStream.Free;
  end;
  FAutor:= 'Salvador Jover';
  FMarca:= '� 2017 El blog de Delphi B�sico';
  FDistanceToCenterAutor:= 115;
  FDistanceToCenterMarca:= 75;
end;

destructor TAdornosProp.Destroy;
begin
  FImage.Free;
  FImage:= Nil;
  FAutor:= '';
  FMarca:= '';
end;

procedure TAdornosProp.DoNeedParentRepaint;
begin
  if Assigned(FOnNeedRepaint) then FOnNeedRepaint(Self);
end;

procedure TAdornosProp.SetAutor(const Value: String);
begin
  if FAutor <> Value then
  begin
    FAutor := Value;
    DoNeedParentRepaint;
  end;
end;

procedure TAdornosProp.SetDistanceToCenterAutor(const Value: Integer);
begin
  if FDistanceToCenterAutor <> Value then
  begin
    FDistanceToCenterAutor := Value;
    DoNeedParentRepaint;
  end;
end;

procedure TAdornosProp.SetDistanceToCenterMarca(const Value: Integer);
begin
  if FDistanceToCenterMarca <> Value then
  begin
    FDistanceToCenterMarca := Value;
    DoNeedParentRepaint;
  end;
end;

procedure TAdornosProp.SetFImage(const Value: TBitmap);
begin
  FImage.Assign(Value);
  DoNeedParentRepaint;
end;

procedure TAdornosProp.SetMarca(const Value: String);
begin
  if Value <> FMarca then
  begin
    FMarca := Value;
    DoNeedParentRepaint;
  end;
end;

procedure TAdornosProp.SetOnNeedRepaint(const Value: TNotifyEvent);
begin
  FOnNeedRepaint := Value;
end;

//initialization
// ReportMemoryLeaksOnShutdown:= True;


end.

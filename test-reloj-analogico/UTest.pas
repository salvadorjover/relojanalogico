unit UTest;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.DateTimeCtrls, FMX.Colors, FMX.ListBox, MiReloj;

type
  TForm1 = class(TForm)
    Switch1: TSwitch;
    grbAdornos: TGroupBox;
    chbExtra: TCheckBox;
    chbLogo: TCheckBox;
    chbDialSegundos: TCheckBox;
    chbDialMinutos: TCheckBox;
    chbDialHoras: TCheckBox;
    chbEsfera: TCheckBox;
    chbBoton: TCheckBox;
    chbAutor: TCheckBox;
    chbMarca: TCheckBox;
    labActivo: TLabel;
    TimeEdit1: TTimeEdit;
    Label1: TLabel;
    ColorPanel1: TColorPanel;
    ColorComboBox1: TColorComboBox;
    Label2: TLabel;
    ColorComboBox2: TColorComboBox;
    Panel1: TPanel;
    MiReloj1: TMiReloj;
    procedure FormCreate(Sender: TObject);
    procedure Switch1Switch(Sender: TObject);
    procedure chbExtraChange(Sender: TObject);
    procedure chbLogoChange(Sender: TObject);
    procedure chbDialSegundosChange(Sender: TObject);
    procedure chbDialMinutosChange(Sender: TObject);
    procedure chbDialHorasChange(Sender: TObject);
    procedure chbEsferaChange(Sender: TObject);
    procedure chbBotonChange(Sender: TObject);
    procedure chbAutorChange(Sender: TObject);
    procedure chbMarcaChange(Sender: TObject);
    procedure TimeEdit1Change(Sender: TObject);
    procedure ColorPanel1Change(Sender: TObject);
    procedure ColorComboBox1Change(Sender: TObject);
    procedure ColorComboBox2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}


procedure TForm1.chbExtraChange(Sender: TObject);
begin
  case chbExtra.IsChecked of
    True: MiReloj1.Adornos:= MiReloj1.Adornos + [paExtra];
    False: MiReloj1.Adornos:= MiReloj1.Adornos - [paExtra];
  end;
end;

procedure TForm1.chbAutorChange(Sender: TObject);
begin
  case chbAutor.IsChecked of
    True: MiReloj1.Adornos:= MiReloj1.Adornos + [paAutor];
    False: MiReloj1.Adornos:= MiReloj1.Adornos - [paAutor];
  end;
end;

procedure TForm1.chbBotonChange(Sender: TObject);
begin
  case chbBoton.IsChecked of
    True: MiReloj1.Adornos:= MiReloj1.Adornos + [paBoton];
    False: MiReloj1.Adornos:= MiReloj1.Adornos - [paBoton];
  end;
end;

procedure TForm1.chbDialHorasChange(Sender: TObject);
begin
  case chbDialHoras.IsChecked of
    True: MiReloj1.Adornos:= MiReloj1.Adornos + [paDialHoras];
    False: MiReloj1.Adornos:= MiReloj1.Adornos - [paDialHoras];
  end;
end;

procedure TForm1.chbDialMinutosChange(Sender: TObject);
begin
  case chbDialMinutos.IsChecked of
    True: MiReloj1.Adornos:= MiReloj1.Adornos + [paDialMinutos];
    False: MiReloj1.Adornos:= MiReloj1.Adornos - [paDialMinutos];
  end;
end;

procedure TForm1.chbDialSegundosChange(Sender: TObject);
begin
  case chbDialSegundos.IsChecked of
    True: MiReloj1.Adornos:= MiReloj1.Adornos + [paDialSegundos];
    False: MiReloj1.Adornos:= MiReloj1.Adornos - [paDialSegundos];
  end;
end;

procedure TForm1.chbEsferaChange(Sender: TObject);
begin
  case chbEsfera.IsChecked of
    True: MiReloj1.Adornos:= MiReloj1.Adornos + [paEsfera];
    False: MiReloj1.Adornos:= MiReloj1.Adornos - [paEsfera];
  end;
end;

procedure TForm1.chbLogoChange(Sender: TObject);
begin
  case chbLogo.IsChecked of
    True: MiReloj1.Adornos:= MiReloj1.Adornos + [paLogo];
    False: MiReloj1.Adornos:= MiReloj1.Adornos - [paLogo];
  end;
end;

procedure TForm1.chbMarcaChange(Sender: TObject);
begin
  case chbMarca.IsChecked of
    True: MiReloj1.Adornos:= MiReloj1.Adornos + [paMarca];
    False: MiReloj1.Adornos:= MiReloj1.Adornos - [paMarca];
  end;
end;

procedure TForm1.ColorComboBox1Change(Sender: TObject);
begin
  MiReloj1.ColorBEsfera:= ColorComboBox1.Color;
end;

procedure TForm1.ColorComboBox2Change(Sender: TObject);
begin
  MiReloj1.ColorExtra:= ColorComboBox2.Color;
end;

procedure TForm1.ColorPanel1Change(Sender: TObject);
begin
  MiReloj1.Fill.Color:= ColorPanel1.Color;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
 fParte: TParteAdorno;
begin
  Switch1.IsChecked:= MiReloj1.EnableTimer;
  if MiReloj1.EnableTimer then
    MiReloj1.SetNewTime(Now)
  else
   MiReloj1.SetNewTime(TimeEdit1.Time);

  for fParte in MiReloj1.Adornos do
  begin
    case fParte of
      paExtra            : chbExtra.IsChecked     := paExtra in MiReloj1.Adornos;
      paLogo             : chbLogo.IsChecked      := paLogo in MiReloj1.Adornos;
      paDialSegundos     : chbDialSegundos.IsChecked:= paDialSegundos in MiReloj1.Adornos;
      paDialMinutos      : chbDialMinutos.IsChecked:= paDialMinutos in MiReloj1.Adornos;
      paDialHoras        : chbDialHoras.IsChecked := paDialHoras in MiReloj1.Adornos;
      paEsfera           : chbEsfera.IsChecked    := paEsfera in MiReloj1.Adornos;
      paBoton            : chbBoton.IsChecked     := paBoton in MiReloj1.Adornos;
      paAutor            : chbAutor.IsChecked     := paAutor in MiReloj1.Adornos;
      paMarca            : chbMarca.IsChecked     := paMarca in MiReloj1.Adornos;
    end;
  end;
  ColorPanel1.Color:= MiReloj1.Fill.Color;
  ColorComboBox1.Color:= MiReloj1.ColorBEsfera;
  ColorComboBox2.Color:= MiReloj1.ColorExtra;
end;

procedure TForm1.Switch1Switch(Sender: TObject);
begin
  MiReloj1.EnableTimer:= Switch1.IsChecked;
  TimeEdit1.Enabled:= not MiReloj1.EnableTimer;
  if TimeEdit1.Enabled then MiReloj1.SetNewTime(TimeEdit1.Time);
end;

procedure TForm1.TimeEdit1Change(Sender: TObject);
begin
  if not MiReloj1.EnableTimer then
    MiReloj1.SetNewTime(TimeEdit1.Time);
end;

end.

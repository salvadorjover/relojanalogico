# README #


### RELOJ ANALOGICO ###

El blog de Delphi Básico - 2017
Autor: Salvador Jover

El código forma parte de una serie de entradas del blog de Delphi 
Básico, con fines exclusivamente didácticos y se entrega tal cual; 
no tiene carácter comercial ni ofrece garantías de que funcione fuera 
del uso meramente educativo. 

Se ha desarrollado con la version Delphi 10.1 Berlin Starter
y mi recomendación es la de que sigas los pasos indicados en la serie
partiendo de un proyecto nuevo, y agregando los módulos necesarios,
principalmente el fichero (PAS) o el fichero (RES), si no deseas escribirlos
o generar la compilación del recurso. 

Introducción a la serie: 
https://community.embarcadero.com/blogs/entry/modulo-de-control-de-presencia-2017-de-la-vcl-a-fmx
Capítulos:
https://delphibasico.com/2017/01/31/modulo-c-p-2017-i-reloj-analogico/
y siguientes (en el blog principal).

Espero que os guste.

